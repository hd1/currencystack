Python wrapper for currencystack.io by Jackson Street Capital LLC -- https://twitter.com/jscapitalLLC
Unit Tests are in tests.py, code is in main.py. 

If you'd like to add functionality:

1. Fork the repository.
2. Add code to tests.py.
3. Make sure the only test that fails is the one you just added.
4. Make the test you added pass by adding code to main.py.
5. Issue a pull request.

