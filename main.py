import json
import requests

class CurrencyStack:
    def __init__(self, api_key):
        self.api_key = api_key

    def lookup(self, currency = None):
        if currency:
            currencies = requests.get('https://raw.githubusercontent.com/xsolla/currency-format/master/currency-format.json').json()
            stanza = [k for k, v in currencies.items() if v['name'].upper() == currency.upper()][0]
            return json.dumps({'currency': currency, 'abbreviation': stanza})
        else:
            currencies = requests.get('https://raw.githubusercontent.com/xsolla/currency-format/master/currency-format.json').json()
            stanza = [k for k, v in currencies.items()]
            return json.dumps(stanza)

    def rate(self, currency1, currency2):
        url = 'https://api.currencystack.io/currency'
        # base=USD&target=USD&apikey=
        params = {'apikey': self.api_key, 'base': str(currency1).upper(), 'target': str(currency2).upper()}

        response_ = requests.get(url, params=params)
        if response_.status_code == 500:
            return {'error': 'Not found'}
        response = response_.json()
        stanza = {'source': currency1.upper(), 'target': currency2.upper(), 'rate': response['rates'][currency2.upper()]}
        return json.dumps(stanza)

