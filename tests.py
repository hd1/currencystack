import json
import six
import unittest

from main import CurrencyStack


class CurrencyStackTests(unittest.TestCase):
    def setUp(self):
        self.instance = CurrencyStack('Q6cf609bebe99ad8d60290118b59cc30')

    def test_resolve(self):
        expected = 'AED'
        long_name = 'UAE Dirham'
        abbrev = json.loads(self.instance.lookup(long_name))['abbreviation']
        self.assertEqual(expected, abbrev)

    def test_resolve_all(self):
        abbrevs = json.loads(self.instance.lookup())
        self.assertEqual(type(abbrevs), type([]))

    def test_identity(self):
        expected = 1
        rate = json.loads(self.instance.rate('USD', 'USD'))['rate']
        self.assertEqual(expected, rate)


if __name__ == '__main__':
    unittest.main()

